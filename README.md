# Homework 3

For this assignment you are to answer the following questions regarding grammars and parsers. This isn't a programming assignment per se, but you might want to experiment with some Rust code to answer some of the questions.

## Submission Instructions

1. Fork this repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Write answers to all of the questions. For parse trees, you may draw these by hand and scan them, draw them in powerpoint and make a picture, or make little ascii trees (probably the easiest). For example:

```
Root
| Parent1
  | Child1
  | Child2
    | GrandChild
  | Child3
| Parent2
```

6. When you're ready to submit indicate this on the corresponding assignment on course site.

## Questions

### Question 1

Write a grammar in EBNF notation to capture the following:

#### a. Strings in Rust. 

Strings begin and end with a quotation mark. They may contain quote or back-slash characters if and only if those are escaped by a preceding backslash.

Valid strings:

- "hello world"
- "abcdefg1234567!@#$%^"
- "This is an escaped quote \" and an escaped backslash \\"

Invalid strings:

- "hello
- "he said "hello" "
- "This \ that"

Answer:
<string> ::= "{<letter> | <digit> | <character>}";
<letter> ::= A|B|..|Z|a|b|..|z;
<digit> ::= 1|2|3|4|5|6|7|8|9|0;
<character> ::= !|@|#|$|%|^|&|*|(|)|~|`|\\|\"|'|;|:|<|>|?|,|.|/|=|+|-|_;


#### b. Comments in Rust. 

These are delimited by /* and */ or by //

Valid comments:

- // comments can have any character including / and *
- //this is valid
- /* you can do a block like this on one line */

Invalid comments:

- / not valid,
- /* blocks need to be closed, so this is invalid
- 

answer:
<comment> ::= //{<letter> | <digit> | <character>} | /* {<letter> | <digit> | <character>} */;
<letter> ::= A|B|..|Z|a|b|..|z;
<digit> ::= 1|2|3|4|5|6|7|8|9|0;
<character> ::= !|@|#|$|%|^|&|*|(|)|~|`|\\|\"|'|;|:|<|>|?|,|.|/|=|+|-|_|"|\;

#### c. E-mail addresses. 

Use this as a guide for how e-mail addresses are formatted: http://www.huge.org/clapres/cla_4.html

Valid e-mail addresses:

- hello@world.com
- cim310@lehigh.edu
- info@bbc.co.uk

Invalid addresses:

- can't have spaces@gmail.com
- this.com
- name@.com

Answer:
<email> ::= {<letter> | <digit> | <character>} @ {<letter> | <digit> | <character>} <address>;
<letter> ::= A|B|..|Z|a|b|..|z;
<digit> ::= 1|2|3|4|5|6|7|8|9|0;
<character> ::= !|@|#|$|%|^|&|*|(|)|~|`|\\|\"|'|;|:|<|>|?|,|.|/|=|+|-|_|"|\;
<address> ::= .com|.org|.edu|.gov|.uk|.nl|...|.de;


#### d. Phone numbers

Use this as a guide for how phone numbers are formatted: https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers#United_States,_Canada,_and_other_NANP_countries

Valid phone numbers:

- 555-555-5555
- 123-456-7890

Invalid phone numbers:

- 484-023-9378
- 123-123-1234
- 5555555555

Answer:
<phone number> ::= <NPA> <NXX> <XXXX>;
<NPA> ::= 484|541|...|808;
<NXX>  ::= <N> <digit> <digit>;
<XXXX> ::= <digit><digit><digit><digit>;
<digit> ::= 1|2|3|4|5|6|7|8|9|0;
<N> ::= 2|3|4|5|6|7|8|9;
#### e. Numeric constants in Rust. 

These are binary, octal, decimal, hexadecimal, scientific.

- Binary numbers are prefixed with 0b and contain the digits 0 and 1. e.g. 0b01010110
- Octal numbers are prefixed with 0o and contain the digits 0 through 7. e.g. 0o12345670
- Decimal numbers have no prefix and contain the digits 0 through 9. An optional period can be inserted to indicate a floating point constant. They can optionally be negative indicated by a preceding - sign. e.g. 1234567890 or 12345.67890
-  Hexadecimal number are prefixed with 0x and contain the digits 0-9, and the letters A-F. e.g. 0x1234567ABCDEF
- Decimal constants can be written in scientific notation of the form: AeN (A times ten raised to the power of N), where the exponent N is an integer, and the coefficient A is any real number. The integer N is called the order of magnitude and the real number A is called the mantissa. 

Optional underscores can be inserted into the middle of a number to improve readability. e.g. 1_000_000

Valid numeric constants:

- 0b1010101
- 0o1643
- 0o1010101
- 456
- 4.56e2
- 3.14159
- 0xFFAA19
- 1_000
- -123.456
- -123

Invalid numbers:

- 0b123
- 0xABCDEFG
- FFAA12
- 0o3.1415
- _1000
- 0000000123
- -0b10101

Answer:
<constants> ::= <binary>|<Octal>|<Decimal>|<Hexadecimal>;
<binary> ::= 0B <bin>;
<bin> ::= 1|0;
<Octal> ::= 0o <oct>;
<oct> ::= 1|2|3|4|5|6|7|0;
<Decimal> ::= (- <digit>|.|<scientific>)|<digit>|.|<scientific>;
digit ::= 1|2|3|4|5|6|7|8|9|0|;
<scientific> ::= <digit> e <digit>;
<Hexadecimal> ::= 0x <hex>;
<hex> ::= 1|2|3|4|5|6|7|8|9|0|A|B|C|D|E|F;

### Question 2

Errors in a program can be classified according to when they are detected and, if they are detected at compile time, what part of the compiler detects them. Using Rust, give an example of each of the following:

1. A lexical error, detected by the scanner.
1. int val+ (you cant use + as an identifier)
2. A syntax error, detected by the parser.
2. int val (there is no ; so there will be an error)
3. A static semantic error, detected by semantic analysis.
3. an error like trying to use a decimal in an array index like arr[2.4]
4. A ownership error, detected by the borrow checker.
4. let myval: Val<String> = val![
  String::from("hello world),
];
let mut otherval: Val<String> = Val::new();
let val = *myval.get(0).unwrap();
(this error tries to transfer ownership of the borowed myval.get(0) to val which is not possible)
5. A lifetime error, detected through lifetime analysis of variables.
5.fn main()
{
    {
        let r;
            {

                r = 6;
            }
    }
println!("r: {}", r);
}
rust variables live withing their {} when a different line outside the {} calls the variable in the {} it does not find the variable at all.

### Question 3

In Rust, what is the limit on the value of a usize? What happens in the event of arithmetic overflow? Don't look this up, try and figure it out on your own. What are the implications of size limits on the portability of programs from one machine/compiler to another?
Answer:
the value of the limit of usize is dependant on the system if you have a 32bit system it will be 32 bits and if you have a 64 bit system it will have 64 bits.
in the event of arithmethic overflow when a value overflows it resets and counts from 0 again.
when transporting code with a usize in it from one computer to another it could change the limit of the usize witch can cause overflow if you go from a 64bit system to a 32bit system this can effect the portability of your code.
### Question 4

Why is it difficult to tell whether a program is correct or not? How do you personally go about finding bugs in your code? What kinds of bugs are revealed by testing? What kind of bust cannot be caught by testing?
Answer: 
when checking code it can be very hard to realise where in the code there is a problem especialy if that problem doesn't give an error but rater doesn't calculate things right.
one thing I do to test my code is to add print statements to print out the value of what I am trying to do or calculate for each step of the operation. this can take a while but it does help me realise where my errors are.
Testing can reveal where there are errors or miss calculations if you write detailed tests it can test every step of your code to see where it is going wrong.
errors that make your code crash can sometimes not be caught by testing because it is hard to see what make the code crash and where it crashed.

### Question 5

Consider the following grammar:

e stands for the string terminating character.

```ebnf
statement = assignment | subroutine_call;
assignment = identifier, ":=", expression;
subroutine_call = identifier, "(", argument_list, ")";
expression = primary, expression_tail;
expression_tail = operator, expression | e;
primary = identifier | subroutine_call | "(" , expression , ")";
operator = "+" | "-" | "*" | "/";
argument_list = expression, argument_tail;
argument_tail = "," , argument_list | e;
identifier = {a-z | A-Z | 0-9};
```

1. Construct a parse tree for the input string "print(x)".
1. Statement --> subroutine_call --> identifier --> argument_list -->expression -->primary --> identifier --> expresson_tail --> argumet_tail
2. Construct a parse tree for the input string "y := add(a,b)".
2. statement --> assignment --> identifier --> expression --> primary --> subroutine_call --> identifier --> argument_list --> expression --> --> primary --> identifier --> expression_tail -->argument tail --> argument_list -->
expression --> primary --> identifier -->  argumetn_tail
3. Construct a parse tree for the input string "z := 1 + 2 * 3".
3. statement --> assignment --> identifier --> expression --> primary --> identifier --> expression_tail -->operator -->expression--> primary --> identifier --> expression_tail --> operator -->expression--> primary --> identifier --> expression_tail
4. Construct a parse tree for the input string "x := 9 * sin(x)".
4. statement --> assignment --> identifier --> expression --> primary --> identifier --> expression_tail --> operator --> expression --> primary --> subroutime_call --> identifier --> argument_list
--> expression --> primary --> identifier --> expresion_tail --> argument tail

### Question 6 

Consider the following context-free grammar:

```ebnf
G = G, B | G, N | e;
B = "(", E, ")";
E = E, "(", E, ")" | e;
N = "(", L, "]";
L = L, E | L, "(" | e;
```
answers:
1. first the program either selects balanced or non-balanced then if it is balanced it enters () and goes to E which also ads () and leaves te option to add another (). the non-balanced 
first adds (] then it goes to L which leaves the option to proceed to E or to add (. 
1. Describe in English the language generated by this grammar. Hint: B stands for "balanced", N stands for "non-balanced".
2. G --> N --> L -->L -->E
2. Give a parse tree for the string "((]()".
3. G-->B-->E-->E
3. Give a parse tree for the string "((()))".